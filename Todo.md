Add back to pylintrc:
* C0114, missing-module-docstring
* C0115, missing-class-docstring

Coding:
* test precedence
* test fancy _simplify ```1 * y + x * 0```
* test division like ```y / (y*y) == 1/y``` using gcd
* test latex render
* power using diff expressions in arg and base, incl. test
      