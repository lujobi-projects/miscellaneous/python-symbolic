from symbolic.expression import Expression
from symbolic.atomic.precedences import REAL_PRECEDENCE


class Real(Expression):
    is_numeric = True
    is_simple = True
    precedence = REAL_PRECEDENCE

    def __init__(self, num: float) -> None:
        self.num = num

    def _simplify(self):
        return Real(self.num)

    def derive(self, _):
        return Real(0)

    def plug_in(self, _):
        return Real(self.num)

    def extract_operands(self, op_prec):
        return [self]

    def _expand(self):
        return self

    def contains(self, x: Expression) -> bool:
        return False

    def to_string(self, latex=False) -> str:
        return str(self.num)

    def __hash__(self) -> int:
        return hash(str(self))

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Real):
            return self.num == other.num
        return False
