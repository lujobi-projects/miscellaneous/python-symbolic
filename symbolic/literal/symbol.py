from symbolic.expression import Expression
from .real import Real


class Symbol(Expression):
    is_numeric = False
    is_simple = True
    precedence = 10

    def __init__(self, name: str) -> None:
        self.name = name
        super().__init__()

    def _simplify(self):
        return Symbol(self.name)

    def derive(self, dx):
        if dx.name == self.name:
            return Real(1)
        return Real(0)

    def plug_in(self, plug_in_map):
        if plug_in_map.get(self.name):
            return Real(plug_in_map[self.name])
        return Symbol(self.name)

    def _expand(self):
        return self

    def extract_operands(self, op_prec):
        return [self]

    def to_string(self, latex=False) -> str:
        return str(self.name)

    def contains(self, x: Expression) -> bool:
        return isinstance(x, Expression) and x.name == self.name

    def __hash__(self) -> int:
        return hash(str(self))

    def __eq__(self, other: object) -> bool:
        if isinstance(other, Symbol):
            return self.name == other.name
        return False
