from abc import ABC, abstractmethod, abstractproperty


def sanitize_input(ipt):
    if isinstance(ipt, (int, float)):
        from symbolic.literal.real import Real
        return Real(ipt)
    return ipt


def check_compatibility(self, other, fun):
    if isinstance(other, (int, float)):
        from symbolic.literal.real import Real
        return fun(Real(other), self)
    raise TypeError(
        f'unsupported operand for type {type(self)} and type {type(other)}')


class Expression(ABC):
    @abstractmethod
    def plug_in(self, plug_in_map):
        pass

    @abstractmethod
    def _simplify(self):
        pass

    @abstractmethod
    def derive(self, dx):
        pass

    @abstractmethod
    def _expand(self):
        pass

    @abstractmethod
    def contains(self, x) -> bool:
        pass

    @abstractmethod
    def to_string(self, latex=False) -> str:
        pass

    @abstractmethod
    def __hash__(self) -> int:
        pass



    @abstractproperty
    def is_numeric(self):
        raise NotImplementedError

    @abstractproperty
    def is_simple(self):
        raise NotImplementedError

    @abstractproperty
    def precedence(self):
        raise NotImplementedError
    
    def expand(self):
        return self._expand()._expand()

    def simplify(self):
        return self._simplify()._simplify()

    def __str__(self) -> str:
        return self.to_string()

    def bracket_no(self, no, latex=False):
        """
        puts brackets around numbers if needed
        """
        bracket = no.precedence <= self.precedence
        res = '(' if bracket else ''
        res += no.to_string(latex)
        res += ')' if bracket else ''
        return res

    def __add__(self, other):
        from symbolic import Add
        return Add(self, sanitize_input(other))

    def __radd__(self, other):
        from symbolic import Add
        return check_compatibility(self, other, Add.evaluate)

    def __sub__(self, other):
        from symbolic import Subtract
        return Subtract(self, sanitize_input(other))

    def __rsub__(self, other):
        from symbolic import Subtract
        return check_compatibility(self, other, Subtract.evaluate)

    def __mul__(self, other):
        from symbolic import Multiply
        return Multiply(self, sanitize_input(other))

    def __rmul__(self, other):
        from symbolic import Multiply
        return check_compatibility(self, other, Multiply.evaluate)

    def __truediv__(self, other):
        from symbolic import Rational
        return Rational(self, sanitize_input(other))

    def __rtruediv__(self, other):
        from symbolic import Rational
        return check_compatibility(self, other, Rational.evaluate)

    def __pow__(self, other):
        from symbolic import Power
        return Power(self, sanitize_input(other))

    def __rpow__(self, other):
        from symbolic import Power
        return check_compatibility(self, other, Power.evaluate)

    def __neg__(self):
        from symbolic import Negate
        return Negate(self)
