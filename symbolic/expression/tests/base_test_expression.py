
class BaseTestExpression:
    def test_construction(self):
        raise NotImplementedError

    def test_plug_in(self):
        raise NotImplementedError

    def test__simplify(self):
        raise NotImplementedError

    def test_derive(self):
        raise NotImplementedError

    def test_to_string(self) -> str:
        raise NotImplementedError

    def test_is_numeric(self):
        raise NotImplementedError

    def test_is_simple(self):
        raise NotImplementedError

    def test_precedence(self):
        raise NotImplementedError
