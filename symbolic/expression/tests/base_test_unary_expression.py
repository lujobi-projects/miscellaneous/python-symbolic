from .base_test_expression import BaseTestExpression


class BaseTestUnaryExpression(BaseTestExpression):
    def test_sign(self):
        raise NotImplementedError

    def test_evaluate(self):
        raise NotImplementedError
