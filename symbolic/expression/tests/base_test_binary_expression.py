from .base_test_expression import BaseTestExpression


class BaseTestBinaryExpression(BaseTestExpression):
    def test_sign(self):
        raise NotImplementedError

    def test_evaluate(self):
        raise NotImplementedError
