from abc import abstractproperty, abstractmethod
from symbolic.literal.real import Real
from symbolic.expression import Expression


class BinaryExpression(Expression):
    no1 = None
    no2 = None
    is_numeric = None
    is_simple = False

    def __init__(self, no1: Expression, no2: Expression) -> None:
        self.no1 = no1
        self.no2 = no2
        self.is_numeric = self.no1.is_numeric and self.no2.is_numeric

    @abstractproperty
    def sign(self):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def evaluate(no1, no2):
        raise NotImplementedError

    def _simplify(self):
        no_1_simple = self.no1.simplify()
        no_2_simple = self.no2.simplify()
        if no_1_simple.is_numeric and no_2_simple.is_numeric:
            return Real(self.evaluate(no_1_simple.num, no_2_simple.num))
        return self.__class__(no_1_simple, no_2_simple)

    def plug_in(self, plug_in_map):
        return self.__class__(
            self.no1.plug_in(plug_in_map),
            self.no2.plug_in(plug_in_map))

    def contains(self, x: Expression) -> bool:
        return self.no1.contains(x) or self.no2.contains(x)

    def to_string(self, latex=False) -> str:
        if latex:
            no1_str = self.bracket_no(self.no1, True)
            no2_str = self.bracket_no(self.no2, True)
            return f'{{{no1_str}}}{self.sign}{{{no2_str}}}'
        no1_str = self.bracket_no(self.no1)
        no2_str = self.bracket_no(self.no2)
        return f'{no1_str} {self.sign} {no2_str}'

    def __hash__(self) -> int:
        return hash(str(self))

    def __eq__(self, other: object) -> bool:
        # very sparse ! no associativity taken into consiteration
        if isinstance(other, self.__class__):
            return self.no1._simplify() == other.no1._simplify() and \
                self.no2._simplify() == other.no2._simplify()
        self_simple = self._simplify()
        if isinstance(self_simple, other.__class__):
            return self_simple == other
        return False
