from abc import abstractproperty, abstractmethod
from symbolic.expression import Expression


class UnaryExpression(Expression):
    no = None
    is_numeric = None
    is_simple = False

    def __init__(self, no: Expression) -> None:
        self.no = no
        self.is_numeric = self.no.is_numeric

    @abstractproperty
    def sign(self):
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def evaluate(no):
        pass

    def _simplify(self):
        no_simple = self.no.simplify()
        if no_simple.is_numeric:
            from symbolic import Real
            return Real(self.evaluate(no_simple.num))
        return self.__class__(no_simple)

    def plug_in(self, plug_in_map):
        return self.__class__(self.no.plug_in(plug_in_map))

    def contains(self, x: Expression) -> bool:
        return self.no.contains(x)

    def to_string(self, latex=False) -> str:
        if latex:
            no_str = self.bracket_no(self.no, True)
            return f'{self.sign}({no_str})'
        return f'{self.sign}({str(self.no)})'

    def __hash__(self) -> int:
        return hash(str(self))

    def __eq__(self, other: object) -> bool:
        # very sparse ! no associativity taken into consiteration
        if isinstance(other, self.__class__):
            return self.no._simplify() == other.no._simplify()
        self_simple = self._simplify()
        if isinstance(self_simple, other.__class__):
            return self_simple == other
        return False
