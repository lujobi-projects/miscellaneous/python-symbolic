from symbolic.expression import Expression
from functools import reduce

class Sum(Expression):
    is_numeric = None
    is_simple = False
    sign = '+'
    precedence = 0

    def __init__(self, *sumands) -> None:
        self.sumands = sumands

    def derive(self, dx):
        return self.__class__(*list(map(lambda s: s.derive(dx), self.sumands)))

    def plug_in(self, plug_in_map):
        return self.__class__(*list(map(lambda s: s.plug_in(plug_in_map), self.sumands)))

    def contains(self, x: Expression) -> bool:
        return reduce(lambda acc, s: acc  or s.contains(x), self.sumands, False)




    def _expand(self):
        return None
    
    def _simplify(self):
        return None

    def to_string(self, latex=False) -> str:
        if not latex:
            return ' + '.join(map(lambda s: f'{s.to_string(latex)}', self.sumands))
        return ' + '.join(map(lambda s: f'{{{s.to_string(latex)}}}', self.sumands))

    def __hash__(self) -> int:
        return hash(str(self))

    def __eq__(self, other: object) -> bool:
        # very sparse ! no associativity taken into consiteration
        return False
