from symbolic.expression import BinaryExpression
from symbolic.literal.real import Real
from .subtract import Subtract
from .multiply import Multiply
from .precedences import RATIONAL_PRECEDENCE


class Rational(BinaryExpression):
    sign = '/'
    precedence = RATIONAL_PRECEDENCE

    def derive(self, dx):
        return Rational(Subtract(Multiply(self.no1.derive(dx), self.no2),
                                 Multiply(self.no1, self.no2.derive(dx))),
                        Multiply(self.no2, self.no2))

    @staticmethod
    def evaluate(no1, no2):
        return no1 / no2

    def latex_string(self):
        return f'\\frac{{{self.no1.to_string(True)}}}{{{self.no2.to_string(True)}}}'

    def _simplify(self):
        res = super()._simplify()
        if isinstance(res, Rational):
            if res.no2 == Real(1):
                return res.no1
        return res

    def _expand(self):
        return self
