from symbolic.expression import UnaryExpression
from .precedences import NEGATE_PRECEDENCE


class Negate(UnaryExpression):
    sign = '-'
    precedence = NEGATE_PRECEDENCE

    def derive(self, dx):
        return Negate(self.no.derive(dx))

    @staticmethod
    def evaluate(no):
        return -no

    def latex_string(self):
        if self.no.is_simple:
            return f'{self.sign}{str(self.no)}'
        return super().latex_string()

    def to_string(self, latex=False) -> str:
        if self.no.is_simple and not latex:
            return f'{self.sign}{str(self.no)}'
        return super().to_string(latex)

    def _expand(self):
        return self

    def _simplify(self):
        return self
