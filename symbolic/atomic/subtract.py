from symbolic.expression import BinaryExpression
from symbolic.literal.real import Real
from .negate import Negate
from .add import Add
from .precedences import SUBTRACT_PRECEDENCE
from .utils import to_add_list


class Subtract(BinaryExpression):
    sign = '-'
    precedence = SUBTRACT_PRECEDENCE

    def derive(self, dx):
        return Subtract(self.no1.derive(dx), self.no2.derive(dx))

    @staticmethod
    def evaluate(no1, no2):
        return no1 - no2


    def _expand(self):
        return self

    def _simplify(self):
        return self