from symbolic.expression import BinaryExpression
from .precedences import ADD_PRECEDENCE

class Add(BinaryExpression):
    sign = '+'
    precedence = ADD_PRECEDENCE

    def derive(self, dx):
        return Add(self.no1.derive(dx), self.no2.derive(dx))

    @staticmethod
    def evaluate(no1, no2):
        return no1 + no2

    def _expand(self):
        return self

    def _simplify(self):
        return self
