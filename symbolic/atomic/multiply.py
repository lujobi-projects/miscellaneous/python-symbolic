from symbolic.expression import BinaryExpression, Expression
from symbolic.literal.real import Real
from .add import Add
from .power import Power
from .precedences import MULTIPLY_PRECEDENCE


class Multiply(BinaryExpression):
    sign = '*'
    precedence = MULTIPLY_PRECEDENCE

    def __init__(self, no1: Expression, no2: Expression) -> None:
        if no2.is_numeric and not no1.is_numeric:
            super().__init__(no2, no1)
        elif no2.is_simple and not no1.is_simple:
            super().__init__(no2, no1)
        else:
            super().__init__(no1, no2)

    def derive(self, dx):
        return Add(Multiply(self.no1.derive(dx), self.no2),
                   Multiply(self.no1, self.no2.derive(dx)))

    @staticmethod
    def evaluate(no1, no2):
        return no1 * no2
    

    def latex_string(self):
        sign = '' if self.no1.is_simple and self.no2.is_simple else '\\cdot'
        no1_str = self.bracket_no(self.no1, True)
        no2_str = self.bracket_no(self.no2, True)
        return f'{{{no1_str}}}{sign}{{{no2_str}}}'

    def _expand(self):
        return self

    def _simplify(self):
        return self
