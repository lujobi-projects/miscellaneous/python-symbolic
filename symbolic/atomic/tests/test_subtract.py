from symbolic import Subtract, Real, Symbol
from symbolic.expression.tests import BaseTestBinaryExpression

r1, r2 = Real(1), Real(2)
lx, ly = Symbol('x'), Symbol('y')


class TestSubtract(BaseTestBinaryExpression):
    real_simple = Subtract(r1, r2)
    sym_simple = Subtract(lx, ly)
    mix_simple = Subtract(r1, ly)
    mix_simple_swap = Subtract(lx, r2)

    def test_construction(self):
        assert r1 - r2 == Real(-1)
        assert self.real_simple == Real(-1)
        assert self.real_simple == r1 - r2
        assert self.sym_simple == lx - ly

    def test_plug_in(self):
        assert self.real_simple.plug_in({'x': 1}) == Real(-1)
        assert self.sym_simple.plug_in({'x': 1}) == Subtract(r1, Symbol('y'))
        assert self.sym_simple.plug_in({'y': 2}) == Subtract(Symbol('x'), r2)
        assert self.sym_simple.plug_in({'x': 1, 'y': 2}) == Real(-1)

    def test__simplify(self):
        assert self.real_simple._simplify() == Real(-1)

    def test_derive(self):
        assert self.real_simple.derive(lx) == Real(0)
        assert self.sym_simple.derive(lx) == Real(1)
        assert self.mix_simple.derive(ly) == Real(-1)

    def test_to_string(self) -> str:
        assert str(self.real_simple) == '1 - 2'
        assert str(self.sym_simple) == 'x - y'
        assert str(self.mix_simple) == '1 - y'
        assert str(self.mix_simple_swap) == 'x - 2'

    def test_is_numeric(self):
        assert self.real_simple.is_numeric
        assert not self.sym_simple.is_numeric
        assert not self.mix_simple.is_numeric

    def test_is_simple(self):
        assert not self.real_simple.is_simple

    def test_precedence(self):
        assert self.real_simple.precedence == 2

    def test_sign(self):
        assert self.real_simple.sign == '-'

    def test_evaluate(self):
        assert self.real_simple.evaluate(1, 2) == -1
