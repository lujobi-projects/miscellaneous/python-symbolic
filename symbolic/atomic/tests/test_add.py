from symbolic import Real, Symbol, Add
from symbolic.expression.tests import BaseTestBinaryExpression

r1, r2 = Real(1), Real(2)
lx, ly = Symbol('x'), Symbol('y')


class TestAdd(BaseTestBinaryExpression):
    real_simple = Add(r1, r2)
    sym_simple = Add(lx, ly)
    mix_simple = Add(r1, ly)
    mix_simple_swap = Add(lx, r2)

    def test_construction(self):
        assert r1 + r2 == Real(3)
        assert self.real_simple == Real(3)
        assert self.real_simple == r1 + r2
        assert self.sym_simple == lx + ly

    def test_plug_in(self):
        assert self.real_simple.plug_in({'x': 1}) == Real(3)
        assert self.sym_simple.plug_in({'x': 1}) == Add(r1, Symbol('y'))
        assert self.sym_simple.plug_in({'y': 2}) == Add(Symbol('x'), r2)
        assert self.sym_simple.plug_in({'x': 1, 'y': 2}) == Real(3)

    def test__simplify(self):
        assert self.real_simple._simplify() == Real(3)

    def test_derive(self):
        assert self.real_simple.derive(lx) == Real(0)
        assert self.sym_simple.derive(lx) == Real(1)

    def test_to_string(self) -> str:
        assert str(self.real_simple) == '1 + 2'
        assert str(self.sym_simple) == 'x + y'
        assert str(self.mix_simple) == '1 + y'
        assert str(self.mix_simple_swap) == 'x + 2'

    def test_is_numeric(self):
        assert self.real_simple.is_numeric
        assert not self.sym_simple.is_numeric
        assert not self.mix_simple.is_numeric

    def test_is_simple(self):
        assert not self.real_simple.is_simple

    def test_precedence(self):
        assert self.real_simple.precedence == 1

    def test_sign(self):
        assert self.real_simple.sign == '+'

    def test_evaluate(self):
        assert self.real_simple.evaluate(1, 2) == 3
