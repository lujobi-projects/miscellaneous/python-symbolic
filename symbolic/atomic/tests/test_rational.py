from symbolic import Real, Symbol, Rational
from symbolic.expression.tests import BaseTestBinaryExpression

r1, r2 = Real(2), Real(4)
lx, ly = Symbol('x'), Symbol('y')


class TestRational(BaseTestBinaryExpression):
    real_simple = Rational(r1, r2)
    sym_simple = Rational(lx, ly)
    mix_simple = Rational(r1, ly)
    mix_simple_swap = Rational(lx, r2)

    def test_construction(self):
        assert r1 / r2 == Real(0.5)
        assert self.real_simple == Real(0.5)
        assert self.real_simple == r1 / r2
        assert self.sym_simple == lx / ly

    def test_plug_in(self):
        assert self.real_simple.plug_in({'x': 1}) == Real(0.5)
        assert self.sym_simple.plug_in({'x': 2}) == Rational(r1, Symbol('y'))
        assert self.sym_simple.plug_in({'y': 4}) == Rational(Symbol('x'), r2)
        assert self.sym_simple.plug_in({'x': 2, 'y': 4}) == Real(0.5)

    def test__simplify(self):
        assert self.real_simple._simplify() == Real(0.5)

    def test_derive(self):
        assert self.real_simple.derive(lx) == Real(0)
        assert self.sym_simple.derive(lx) == ly / (ly * ly)
        assert self.sym_simple.derive(ly) == -lx / (ly * ly)
        assert self.mix_simple.derive(ly) == -2 / (ly * ly)
        assert self.mix_simple_swap.derive(lx) == Real(0.25)

    def test_to_string(self) -> str:
        assert str(self.real_simple) == '2 / 4'
        assert str(self.sym_simple) == 'x / y'
        assert str(self.mix_simple) == '2 / y'
        assert str(self.mix_simple_swap) == 'x / 4'

    def test_is_numeric(self):
        assert self.real_simple.is_numeric
        assert not self.sym_simple.is_numeric
        assert not self.mix_simple.is_numeric

    def test_is_simple(self):
        assert not self.real_simple.is_simple

    def test_precedence(self):
        assert self.real_simple.precedence == 4

    def test_sign(self):
        assert self.real_simple.sign == '/'

    def test_evaluate(self):
        assert self.real_simple.evaluate(2, 4) == 0.5
