from symbolic import Real, Symbol, Negate
from symbolic.expression.tests import BaseTestUnaryExpression

r = Real(2)
lx = Symbol('x')


class TestNegate(BaseTestUnaryExpression):
    real_simple = Negate(r)
    sym_simple = Negate(lx)
    real_add = -(r + 3)

    def test_construction(self):
        assert -r == Real(-2)
        assert self.real_simple == Real(-2)
        assert self.real_simple == -r
        assert self.sym_simple == -lx

    def test_plug_in(self):
        assert self.real_simple.plug_in({'x': 1}) == Real(-2)
        assert self.sym_simple.plug_in({'x': 1}) == Real(-1)

    def test__simplify(self):
        assert self.real_simple._simplify() == Real(-2)
        assert self.real_add._simplify() == Real(-5)

    def test_derive(self):
        assert self.real_simple.derive(lx) == Real(0)
        assert self.sym_simple.derive(lx) == Real(-1)

    def test_to_string(self) -> str:
        assert str(self.real_simple) == '-2'
        assert str(self.sym_simple) == '-x'

    def test_is_numeric(self):
        assert self.real_simple.is_numeric
        assert not self.sym_simple.is_numeric

    def test_is_simple(self):
        assert not self.real_simple.is_simple

    def test_precedence(self):
        assert self.real_simple.precedence == 10

    def test_sign(self):
        assert self.real_simple.sign == '-'

    def test_evaluate(self):
        assert self.real_simple.evaluate(2) == -2
