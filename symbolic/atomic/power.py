from symbolic.expression import BinaryExpression
from symbolic.literal.real import Real
from .precedences import POWER_PRECEDENCE


class Power(BinaryExpression):
    sign = '^'
    precedence = POWER_PRECEDENCE

    def derive(self, dx):
        simple_no1 = self.no1.simplify()
        simple_no2 = self.no2.simplify()
        if not simple_no2.contains(dx):
            return Power(simple_no1, simple_no2 - 1) * simple_no1.derive(dx)
        raise NotImplementedError

    @staticmethod
    def evaluate(no1, no2):
        return no1 ** no2

    def latex_string(self):
        bracket_1 = self.no1.precedence < self.precedence
        res = '{' + ('(' if bracket_1 else '')
        res += self.no1.to_string(True)
        res += (')' if bracket_1 else '') + '}'
        res += self.sign
        res += '{' + self.no2.to_string(True) + '}'
        return res

    def _expand(self):
        return self

    def _simplify(self):
        return self
