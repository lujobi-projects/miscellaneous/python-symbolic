format:
	autopep8 --in-place --aggressiv --aggressiv --recursive symbolic/

lint:
	pylint symbolic

test:
	pytest symbolic

mypy:
	mypy symbolic

coverage:
	coverage run -m pytest symbolic && coverage report

coverage_html:
	coverage run -m pytest symbolic && coverage html
