from symbolic import *
w = Symbol('w')
x = Symbol('x')
y = Symbol('y')


#test = (w-5)*(w-4)
#test_extr = test.extract_operands(3)
#print(test.expand())
#print(test.simplify())
"""
res = ''
for i in set(test_extr):
    res += f'{i}, '
print(res)
print(test_extr.count(x))
print(test_extr.count(w))
print(test_extr.count(w-4))
"""
test = (x-(2*x-w))
test = (x+(-(2+x+w)))
#test = (x+(-3*x))
#test = (x-2*x)
print(test.expand())
ex = test.expand()
print(ex.simplify())

su = Sum(x, x, (x+x), x*(x+1), test)
print(su)
print(su.derive(x))
print(su.to_string(latex=True))

"""
rat = x * (x + w + x)
print(rat)
print(rat.expand())
rat = (w + x) * (w + x)
print(rat)
print(rat._expand())
rat = (w + x + y) * (w + x + y)
print(rat)
print(rat.expand())
rat = (w + x) * (w + x) * (w + x) * (w + x)
print(rat)
print(rat.expand())
"""


"""rat3 = (w+x)/(x+x)
print(rat3)
print(rat3.plug_in({'w': 3, 'x': 5}))
print(rat3.plug_in({'w': 3, 'x': 5})._simplify())
print(rat3.plug_in({'x': 5}))
print(rat3.plug_in({'x': 5})._simplify())

der_1 = x * x
print(der_1.derive(x))
print(der_1.derive(x)._simplify())

trying = 2*(x** (w*2))
print(trying)
print(trying.to_string(True))
print(trying._simplify())
print(trying.derive(x)._simplify())

trying = (-2*(x-4))/(x** (w*2))
print(trying)
print(trying.to_string(True))

trying = x - (w + 2)
print(trying)
trying = x + x + x + x
print(trying._simplify())
trying = x - x + x + x
print(trying._simplify())"""